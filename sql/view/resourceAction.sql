-- --defaults-group-suffix=Phiscal
-- -t
--
DROP VIEW IF EXISTS `v_resourceAction`;

CREATE VIEW v_resourceAction AS
SELECT
	r.NAME resource,
	method,
	route,
	a.name action,
	preflight,
	a.description,
	private
FROM
		action a
INNER JOIN	resource r
USING (`RESOURCE_ID`)
ORDER BY
	resource,
	route
