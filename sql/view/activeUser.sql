-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_activeUser AS
SELECT
	USER_ID id,
	username,
	CONCAT(naam(FIRSTNAME), ' ', naam(LASTNAME)) fullname,
	naam(FIRSTNAME) firstname,
	naam(LASTNAME) lastname,
	idnumber,
	email,
	phone,
	avatar
FROM user
WHERE ACTIVE;
