-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_module AS
SELECT
	mod_id,
	name,
	description,
	webclient,
	logo,
	DATEDIFF(NOW(),DATE) days,
	s.active,
	tenant_id
FROM
	module m
INNER JOIN
	subscription s
	USING (MOD_ID)
ORDER BY
	s.DATE
	DESC
