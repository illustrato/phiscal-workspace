-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_user_tenant AS
(
	SELECT TENANT_ID, USER_ID, 'A' RANK FROM tenant
)
UNION
(
	SELECT TENANT_ID, a.USER_ID, 'A' RANK FROM admin a INNER JOIN tenant USING(`ORG_ID`)
)
UNION
(
	SELECT TENANT_ID, e.USER_ID, 'E' RANK FROM employee e INNER JOIN tenant USING(`ORG_ID`)
)
