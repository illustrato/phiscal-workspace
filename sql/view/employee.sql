-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_staff AS
SELECT
	user_id,
	CONCAT(naam(FIRSTNAME), ' ', naam(LASTNAME),' ',IF(`DOB` IS \N,'',CONCAT('(',TIMESTAMPDIFF(YEAR, `DOB`, CURDATE()),')'))) fullname,
	naam(FIRSTNAME) firstname,
	naam(LASTNAME) lastname,
	TIMESTAMPDIFF(YEAR, `DOB`, CURDATE()) AS age,
	dob,
	idnumber,
	u.email,
	phone,
	nationality,
	avatar,
	active,
	e.ORG_ID organization,
	e.EMAIL workEmail,
	e.TELEPHONE workTelephone,
	e.BRANCH_NO empBranch,
	b.addressline1 branchAddress,
	b.city branchCity,
	b.region branchRegion,
	b.country branchCountry,
	s.state,
	s.report
FROM account_state s
INNER JOIN user u
	ON u.active = s.ACCST_ID
INNER JOIN employee e
	USING(USER_ID)
LEFT JOIN branch b
	USING(ORG_ID,BRANCH_NO);
