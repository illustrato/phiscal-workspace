-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_profilePermission AS
SELECT
	p.PROFILE_ID	id,
	p.NAME		profile,
	r.NAME		resource,
	a.ROUTE		action,
	a.DESCRIPTION	description
FROM
	profile p
	INNER JOIN permission m USING (`PROFILE_ID`)
	INNER JOIN action a USING (`ACTION_ID`)
	INNER JOIN resource r USING (`RESOURCE_ID`)
ORDER BY
	profile,
	resource,
	action
