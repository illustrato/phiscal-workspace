-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_tenant AS
SELECT
	tenant_id,
	type,
	JSON_OBJECT
	(
		'tenant_id', tenant_id,
		'user_id', user_id,
		'type', type,
		'org_id', t.org_id,
		'website', (SELECT WEBSITE FROM organization WHERE ORG_ID = t.org_id),
		'trademark', trademark(tenant_id),
		'face', face(tenant_id)
	) business,
	tenantmodules(tenant_id) modules
FROM
	tenant t
