-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_org_depth AS
SELECT
	a.`USER_ID` admin,
	t.tenant_id,
	o.org_id,
	o.name,
	o.struct_id,
	o.dba,
	o.description,
	o.motto,
	o.regno,
	s.name structure,
	o.registra,
	o.website,
	l.LOGO_NO logo,
	l.ext,
	l.width,
	l.height,
	l.main mainLogo,
	l.size,
	l.description logoDesc,
	b.BRANCH_NO branch,
	b.photo,
	b.main mainBranch,
	b.addressline1,
	b.addressline2,
	b.city,
	b.region,
	b.country,
	b.gps,
	b.telephone,
	b.description branchDesc,
	(SELECT COUNT(*) FROM employee WHERE ORG_ID = o.ORG_ID AND BRANCH_NO = b.BRANCH_NO) staff,
	e.*,
	p.name gatewayName,
	g.provider_id gatewayProvider,
	g.client_id gatewayClient,
	g.access_key gatewayKey
FROM
	admin a
INNER JOIN tenant t
	USING(`ORG_ID`)
LEFT JOIN paymentgateway g
	USING(`TENANT_ID`)
LEFT JOIN paygateprovider p
	USING(`PROVIDER_ID`)
INNER JOIN organization o
	USING(`ORG_ID`)
INNER JOIN structure s
	USING(`STRUCT_ID`)
LEFT JOIN logo l
	USING (`ORG_ID`)
LEFT JOIN branch b
	USING (`ORG_ID`)
LEFT JOIN v_staff  e
	ON b.ORG_ID = e.organization;
