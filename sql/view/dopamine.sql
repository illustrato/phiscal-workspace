-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_dopamine AS
SELECT JSON_OBJECT(
	'id', n.not_count,
	'type', n.type,
	'icon', n.icon,
	'message', n.message,
	'time', n.time,
	'read', n.have_read,
	'trademark', CONCAT(m.mod_id,'.png'),
	'module', m.name
	) feed,
	TIME,
	USER_ID
FROM
	notification n
LEFT JOIN module m
	USING (MOD_ID)
