-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_session AS
SELECT	JSON_OBJECT(
	'id', USER_ID,
	'username', username,
	'fullname', CONCAT(naam(FIRSTNAME), ' ', naam(LASTNAME)),
	'firstname', naam(FIRSTNAME),
	'lastname', naam(LASTNAME),
	'idnumber', idnumber,
	'email', email,
	'phone', phone,
	'avatar', avatar,
	'active', active,
	'password', password IS \N,
	'tenants', tenants(u.`user_id`),
	'employee', USER_ID IN (SELECT USER_ID FROM employee)
	) user,
	USER_ID
FROM user u;
