-- --defaults-group-suffix=Phiscal
-- -t
--
CREATE OR REPLACE VIEW v_org_compact AS
SELECT JSON_OBJECT(
	'admin', a.`USER_ID`,
	'tenant',t.tenant_id,
	'id', o.org_id,
	'name', trademark(t.tenant_id),
	'regno', o.regno,
	'website', o.website,
	'description', o.description,
	'motto', o.motto,
	'registra', o.registra,
	'logo', CONCAT(l.LOGO_NO,'.',l.EXT)
	) organization,
	o.ORG_ID
FROM
	admin a
INNER JOIN tenant t
	USING(`ORG_ID`)
INNER JOIN organization o
	USING(`ORG_ID`)
LEFT JOIN structure s
	USING(`STRUCT_ID`)
LEFT JOIN logo l
	ON l.ORG_ID = o.ORG_ID AND l.MAIN;
