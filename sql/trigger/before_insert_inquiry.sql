-- --defaults-group-suffix=Phiscal
-- -t
--
DROP TRIGGER IF EXISTS `before_insert_inquiry`;
DELIMITER $$
CREATE TRIGGER `before_insert_inquiry`
BEFORE INSERT ON `inquiry`
FOR EACH ROW
BEGIN

 DECLARE fname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.FIRST_NAME));
 DECLARE lname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.LAST_NAME));
 DECLARE umail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.EMAIL));
 DECLARE uphone VARCHAR(15) DEFAULT TRIM(NEW.PHONE);
 DECLARE topic VARCHAR(90) DEFAULT TRIM(NEW.SUBJECT);
 DECLARE body text DEFAULT TRIM(NEW.CONTENT);

 SET NEW.FIRST_NAME = IF(fname = '', \N, fname);
 SET NEW.LAST_NAME = IF(lname = '', \N, lname);
 SET NEW.PHONE = IF(uphone = '', \N, uphone);
 SET NEW.EMAIL = IF(umail = '', \N, umail);
 SET NEW.SUBJECT = IF(topic = '', \N, topic);
 SET NEW.CONTENT = IF(body = '', \N, body);

END $$

DELIMITER ;
