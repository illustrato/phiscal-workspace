-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE TRIGGER `before_insert_payment`
BEFORE INSERT ON `paymentgateway`
FOR EACH ROW
BEGIN

 DECLARE processor CHAR(2) CHARSET UTF8 DEFAULT TRIM(NEW.PROVIDER_ID);
 DECLARE clientId VARCHAR(35) DEFAULT TRIM(NEW.CLIENT_ID);
 DECLARE access VARCHAR(255) DEFAULT TRIM(NEW.ACCESS_KEY);

 SET NEW.PROVIDER_ID = IF(processor = '', \N, processor);
 SET NEW.CLIENT_ID = IF(clientId = '', \N, clientId);
 SET NEW.ACCESS_KEY = IF(access = '', \N, access);

END $$

DELIMITER ;

DELIMITER $$
CREATE OR REPLACE TRIGGER `before_update_payment`
BEFORE UPDATE ON `paymentgateway`
FOR EACH ROW
BEGIN

 DECLARE processor CHAR(2) CHARSET UTF8 DEFAULT TRIM(NEW.PROVIDER_ID);
 DECLARE clientId VARCHAR(35) DEFAULT TRIM(NEW.CLIENT_ID);
 DECLARE access VARCHAR(255) DEFAULT TRIM(NEW.ACCESS_KEY);

 SET processor = IF(IFNULL(processor, '') = '',  OLD.PROVIDER_ID, processor);
 SET clientId = IF(IFNULL(clientId, '') = '', OLD.CLIENT_ID, clientId);
 SET access = IF(IFNULL(access, '') = '', OLD.ACCESS_KEY, access);

 SET NEW.PROVIDER_ID = IF(processor = '?', \N, processor);
 SET NEW.CLIENT_ID = IF(clientId = '?', \N, clientId);
 SET NEW.ACCESS_KEY = IF(access = '?', \N, access);

END $$

DELIMITER ;
