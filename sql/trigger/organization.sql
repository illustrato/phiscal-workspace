-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE TRIGGER `before_insert_orginzation`
BEFORE INSERT ON `organization`
FOR EACH ROW
BEGIN

 DECLARE orgname VARCHAR(45) DEFAULT TRIM(NEW.NAME);
 DECLARE regno VARCHAR(21) DEFAULT TRIM(NEW.REGNO);
 DECLARE commission VARCHAR(85) DEFAULT TRIM(NEW.REGISTRA);
 DECLARE web VARCHAR(35) DEFAULT LOWER(TRIM(NEW.WEBSITE));
 DECLARE slogan VARCHAR(255) DEFAULT TRIM(NEW.MOTTO);

 SET NEW.NAME = IF(orgname = '', \N, orgname);
 SET NEW.REGNO = IF(regno = '', \N, regno);
 SET NEW.REGISTRA = IF(commission = '', \N, commission);
 SET NEW.WEBSITE = IF(web = '', \N, web);
 SET NEW.MOTTO = IF(slogan = '', \N, slogan);

END $$
DELIMITER ;

DELIMITER $$
CREATE OR REPLACE TRIGGER `before_update_orginzation`
BEFORE UPDATE ON `organization`
FOR EACH ROW
BEGIN

 DECLARE commission VARCHAR(85) DEFAULT TRIM(NEW.REGISTRA);
 DECLARE dba VARCHAR(65) DEFAULT TRIM(NEW.DBA);
 DECLARE info VARCHAR(255) DEFAULT TRIM(NEW.DESCRIPTION);
 DECLARE orgname VARCHAR(45) DEFAULT TRIM(NEW.NAME);
 DECLARE regno VARCHAR(21) DEFAULT TRIM(NEW.REGNO);
 DECLARE slogan VARCHAR(255) DEFAULT TRIM(NEW.MOTTO);
 DECLARE struct CHAR(3) CHARSET UTF8 DEFAULT TRIM(NEW.STRUCT_ID);
 DECLARE web VARCHAR(35) DEFAULT LOWER(TRIM(NEW.WEBSITE));

 SET commission = IF(IFNULL(commission, '') = '', OLD.REGISTRA , commission);
 SET dba = IF(IFNULL(dba, '') = '', OLD.DBA, dba);
 SET info = IF(IFNULL(info, '') = '', OLD.DESCRIPTION, info);
 SET orgname = IF(IFNULL(orgname, '') = '', OLD.NAME , orgname);
 SET regno = IF(IFNULL(regno, '') = '', OLD.REGNO , regno);
 SET slogan = IF(IFNULL(slogan, '') = '', OLD.MOTTO, slogan);
 SET struct = IF(IFNULL(struct, '') = '', OLD.STRUCT_ID, struct);
 SET web = IF(IFNULL(web, '') = '', OLD.WEBSITE , web);

 SET NEW.DESCRIPTION = IF(info = '?', \N, info);
 SET NEW.DBA = IF(dba = '?', \N, dba);
 SET NEW.MOTTO = IF(slogan = '?', \N, slogan);
 SET NEW.NAME = IF(orgname = '?', OLD.NAME, orgname);
 SET NEW.REGNO = IF(regno = '?', \N, regno);
 SET NEW.STRUCT_ID = IF(struct = '?', \N, struct);
 SET NEW.REGISTRA = IF(commission = '?', \N, commission);
 SET NEW.WEBSITE = IF(web = '?', \N, web);

 SET NEW.MODIFIED = NOW();

END $$
DELIMITER ;
