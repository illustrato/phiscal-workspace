-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `subscribe`
(
	IN tenantId 	BIGINT UNSIGNED,
	IN modId	CHAR(2) CHARSET UTF8,
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	INT,
	OUT info 	TEXT
)
BEGIN
	DECLARE latest DATETIME DEFAULT (SELECT MAX(DATE) FROM subscription WHERE TENANT_ID = tenantId AND MOD_ID = modId);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
	END;
	IF NOT auth(tenantId,client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "you're not authorized for operation", MYSQL_ERRNO = 30000;
	END IF;

	IF latest IS NULL || DATEDIFF(NOW(),latest) > 30 THEN
		INSERT INTO subscription VALUES(tenantId, modId, NOW() ,1 , TRUE);
	ELSE
		UPDATE subscription SET ACTIVE = NOT ACTIVE WHERE DATE = latest;
	END IF;

	SET info = latest;
	SET code = 0;
END $$
DELIMITER ;
