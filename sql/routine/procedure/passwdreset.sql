-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `passwdreset`
(
	IN uId		CHAR(12) CHARSET UTF8,
	IN authentic	BOOLEAN,
	IN tkn		TEXT,
	IN passwd	CHAR(60) CHARSET UTF8,
	IN ip		CHAR(15),
	IN agent	TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE userFound	BOOLEAN DEFAULT (SELECT COUNT(USER_ID) FROM user WHERE USER_ID = uId);
	DECLARE hasToken	BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE userFound && TOKENTYPE_ID = 'PSD');
	DECLARE tokenValid	BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE hasToken && TOKEN = tkn);
	DECLARE hasPasswd	BOOLEAN DEFAULT (SELECT PASSWORD IS NOT \N FROM user WHERE USER_ID = uId);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT userFound THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'User Not Found', MYSQL_ERRNO = 3000;
	END IF;
	IF hasToken && NOT tokenValid THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Token', MYSQL_ERRNO = 3000;
	END IF;
	IF NOT hasToken && hasPasswd && NOT authentic THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Please provide Correct Password', MYSQL_ERRNO = 3000;
	END IF;

	START TRANSACTION;
		UPDATE user SET PASSWORD = passwd WHERE USER_ID = uId;
		INSERT INTO password_change(USER_ID,IPADDRESS,USERAGENT) VALUES(uId,ip,agent);
		DELETE FROM token WHERE USER_ID = uid AND TOKENTYPE_ID = 'PSD';
	COMMIT;

	SET info = 'Password successfully Changed';
	SET code = 0;
END $$
DELIMITER ;
