-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `crud_freelance`
(
	IN name		VARCHAR(20),
	IN modId	CHAR(2) CHARSET UTF8,
	IN processor	CHAR(2) CHARSET UTF8,
	IN paygateId	VARCHAR(35),
	IN access 	VARCHAR(255),
	IN client 	CHAR(12),
	OUT code 	INT,
	OUT info 	TEXT
)
BEGIN
	DECLARE tenantId BIGINT UNSIGNED;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	DECLARE EXIT HANDLER FOR 1062
	BEGIN
		SELECT TENANT_ID FROM tenant WHERE USER_ID = client INTO tenantId;
		IF IFNULL(processor, '') <> '' THEN
			call paygate(tenantId, processor, paygateId, access, client, code, info);
		END IF;
		IF code <> 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = info, MYSQL_ERRNO = code;
		END IF;
		COMMIT;
		SET info = JSON_OBJECT('fresh', false, 'tenant_id', tenantId, 'report', CONCAT(name, ' information updated'));
		SET code = 0;
	END;
	START TRANSACTION;
		UPDATE user SET ALIAS = name WHERE USER_ID = client;
		INSERT INTO tenant(USER_ID, TYPE) VALUES(client, 'U');
		SET tenantId = LAST_INSERT_ID();
		IF modId IS NOT \N THEN
			INSERT INTO subscription VALUES(tenantId, modId, NOW(), 1, TRUE);
		END IF;
		IF IFNULL(processor, '') <> '' THEN
			call paygate(tenantId, processor, paygateId, access, client, code, info);
		END IF;
		IF code <> 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = info, MYSQL_ERRNO = code;
		END IF;
	COMMIT;
	SET info = JSON_OBJECT('fresh', true, 'tenant_id', tenantId, 'report', CONCAT("You're now a business with name '", name, "'"));
	SET code = 0;
END $$
DELIMITER ;
