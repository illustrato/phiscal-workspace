-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `emailverification`
(
	IN action	VARCHAR(8),
	IN uId		CHAR(12) CHARSET UTF8,
	IN utoken	TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE TOKEN_TYPE CHAR(3) CHARSET UTF8 DEFAULT 'REG';
	DECLARE FOUND BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE TOKENTYPE_ID = TOKEN_TYPE  AND TOKEN = utoken);
	DECLARE fullname VARCHAR(41) DEFAULT(SELECT CONCAT(naam(FIRSTNAME), ' ', naam(LASTNAME)) FROM user WHERE USER_ID = uId);
	DECLARE REF CHAR(12) CHARSET UTF8 DEFAULT(SELECT REFERRER FROM employee WHERE USER_ID = uId);
	DECLARE org VARCHAR(45);

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT FOUND THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'token not found', MYSQL_ERRNO = 3000;
	END IF;

	START TRANSACTION;

		CASE action
			WHEN 'activate' THEN
			BEGIN
				SELECT USERAGENT FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = TOKEN_TYPE INTO info;
				UPDATE user SET ACTIVE = TRUE WHERE USER_ID = uId;
				IF REF IS NOT \N THEN
					INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(REF, 'event', 'fa fa-user text-success', CONCAT(fullname, ' activated their user account.'));
				END IF;
			END;
			WHEN 'decline' THEN
			BEGIN
				SELECT NAME FROM organization INNER JOIN employee USING (ORG_ID) WHERE USER_ID = uId INTO org;
				DELETE FROM employee WHERE USER_ID = uId;
				UPDATE user SET ACTIVE = TRUE WHERE USER_ID = uId;
				INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(REF, 'event', 'fa fa-ban text-danger', CONCAT(fullname, ' rejected being an employee for ', org, '.'));
			END;
			WHEN 'revoke' THEN
			BEGIN
				SELECT CONCAT(USER_ID, '.', AVATAR) FROM user WHERE USER_ID = uId INTO info;
				DELETE FROM user WHERE USER_ID = uId;
				IF REF IS NOT \N THEN
					INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(REF, 'event', 'fa fa-ban text-danger', CONCAT(fullname, ' rejected having a Phiscal Account.'));
				END IF;
			END;
		END CASE;
		DELETE FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = TOKEN_TYPE;

	COMMIT;
	SET code = 0;
END $$
DELIMITER ;
