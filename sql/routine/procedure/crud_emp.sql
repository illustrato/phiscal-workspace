-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `crud_emp`
(
	IN uId		CHAR(12) CHARSET UTF8,
	IN fname	VARCHAR(20),
	IN lname	VARCHAR(20),
	IN nation	CHAR(2),
	IN idno		CHAR(13),
	IN birthday	DATE,
	IN umail	VARCHAR(45),
	IN wmail	VARCHAR(45),
	IN tel		VARCHAR(15),
	IN base		SMALLINT UNSIGNED,
	IN tenantId	BIGINT UNSIGNED,
	IN pic		VARCHAR(5),
	IN client	CHAR(12) CHARSET UTF8,
	IN skipmail	BOOLEAN,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE org BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE admin_name VARCHAR(41) DEFAULT(SELECT fullname FROM v_activeUser WHERE id = client);
	DECLARE admin_mail VARCHAR(45) DEFAULT(SELECT email FROM v_activeUser WHERE id = client);
	DECLARE org_name VARCHAR(45) DEFAULT(SELECT NAME FROM organization WHERE ORG_ID = org);
	DECLARE utoken TEXT DEFAULT MD5(CONCAT(umail,nation));
	DECLARE active BOOLEAN DEFAULT (SELECT ACTIVE FROM user WHERE USER_ID = uId);
	DECLARE TID CHAR(22) CHARSET UTF8;
	DECLARE isEmployed BOOLEAN;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;

	IF NOT auth(tenantId, client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
	END IF;

	IF uid IS \N THEN
	BEGIN
		INSERT INTO user(FIRSTNAME, LASTNAME, NATIONALITY, IDNUMBER, DOB, EMAIL, ACTIVE, AVATAR) VALUES(fname, lname, nation, idno, birthday, umail, skipmail, pic);
		SET uid = @USER_ID;
		INSERT INTO employee(USER_ID, ORG_ID, REFERRER, EMAIL, TELEPHONE) VALUES(uId, org, client, wmail, tel);

		IF NOT skipmail THEN
			SELECT CONCAT(uid,UNIX_TIMESTAMP()) INTO TID;
			INSERT INTO token(TOKEN_ID, USER_ID, TOKEN, TOKENTYPE_ID) VALUES(TID, uId, utoken, 'REG');
		END IF;
	END;
	ELSE
	BEGIN
		SELECT COUNT(*) FROM employee WHERE USER_ID = uId AND ORG_ID = org INTO isEmployed;

		IF NOT isEmployed THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "This is No Employee", MYSQL_ERRNO = 3001;
		END IF;

		UPDATE user
		SET
			FIRSTNAME = fname,
			LASTNAME = lname,
			NATIONALITY = nation,
			IDNUMBER = idno,
			DOB = birthday,
			EMAIL = umail
		WHERE
			USER_ID = uid;

		UPDATE employee
		SET
			EMAIL = wmail,
			TELEPHONE = tel
		WHERE
			USER_ID = uid;

		IF active THEN
			INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(uId, 'event', 'fa fa-user text-success', CONCAT(admin_name ,' updated your profile details.'));
		END IF;
	END;
	END IF;
	IF base > 0 THEN
		UPDATE employee SET BRANCH_NO = base WHERE USER_ID = uid AND ORG_ID = org;
	END IF;

	COMMIT;
	SET code = 0;
	SELECT JSON_OBJECT('business', org_name, 'admin_name', admin_name, 'admin_mail', admin_mail, 'token', utoken) INTO info;
	SELECT * FROM v_staff  WHERE user_id = uId;
END $$
DELIMITER ;
