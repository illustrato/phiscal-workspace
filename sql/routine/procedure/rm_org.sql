-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `rm_org`
(
	IN tenantId	BIGINT UNSIGNED,
	IN confirmation	VARCHAR(45),
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE orgId BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE orgName VARCHAR(45) DEFAULT (SELECT NAME FROM organization WHERE ORG_ID = orgId);
	DECLARE authorized BOOLEAN DEFAULT auth(tenantId, client);

	DECLARE empId CHAR(12) CHARSET UTF8;
	DECLARE done BOOLEAN DEFAULT FALSE;
	DECLARE cursor_staff CURSOR FOR SELECT e.USER_ID FROM employee e INNER JOIN user u USING(USER_ID) WHERE ORG_ID = orgId AND ACTIVE;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT authorized THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
	END IF;

	IF LOWER(confirmation) <> LOWER(orgName) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Confirmation Failed! Check Your Spelling.", MYSQL_ERRNO = 3001;
	END IF;

	START TRANSACTION;
--
-- Inform Employees
--
		OPEN cursor_staff;
		read_loop: LOOP
			FETCH cursor_staff INTO empId;
			IF done THEN
				LEAVE read_loop;
			END IF;
			INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(empId, 'event', 'fa fa-building text-danger', CONCAT(trademark(tenantId) , ' has been deleted from Phiscal by its Administration.'));
		END LOOP;
		CLOSE cursor_staff;
--
-- Perform Delete
--
		DELETE FROM organization WHERE ORG_ID = orgId;

	COMMIT;
	SET info = orgId;
	SET code = 0;
END $$
DELIMITER ;
