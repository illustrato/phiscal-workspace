-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `paygate`
(
	IN tenantId	BIGINT UNSIGNED,
	IN processor	CHAR(2) CHARSET UTF8,
	IN clientId	VARCHAR(35),
	IN access 	VARCHAR(255),
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	DECLARE EXIT HANDLER FOR 1062
	BEGIN
		UPDATE paymentgateway SET PROVIDER_ID = processor, CLIENT_ID = clientId, ACCESS_KEY = access WHERE TENANT_ID = tenantId;
		SET code = 0;
	END;

	IF NOT auth(tenantId, client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to view tenant", MYSQL_ERRNO = 3000;
	END IF;

	IF processor <> '?' THEN
		INSERT INTO paymentgateway VALUES(processor, tenantId, clientId, access);
	ELSE
		DELETE FROM paymentgateway WHERE TENANT_ID = tenantId;
	END IF;
	SET code = 0;
END $$
DELIMITER ;
