-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `mainswitch`
(
	IN tenantId	BIGINT UNSIGNED,
	IN target	VARCHAR(15),
	IN id		TINYINT UNSIGNED,
	IN action	BOOLEAN,
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE org BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION, 1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT auth(tenantId, client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
	END IF;

	START TRANSACTION;
		CASE target
			WHEN 'branch' THEN
				UPDATE branch SET MAIN = FALSE WHERE ORG_ID = org;
				UPDATE branch SET MAIN = action WHERE ORG_ID = org AND BRANCH_NO = id;
				SELECT BRANCH_NO branch, main, photo, addressline1, addressline2, city, country, gps, telephone, description FROM branch WHERE ORG_ID = org;
			BEGIN
			END;
			WHEN 'logo' THEN
				UPDATE logo SET MAIN = FALSE WHERE ORG_ID = org;
				UPDATE logo SET MAIN = action WHERE ORG_ID = org AND LOGO_NO = id;
				SELECT LOGO_NO id, CONCAT(LOGO_NO,'.',EXT) file, size, main, description, width, height, ext FROM logo WHERE ORG_ID = org;
			BEGIN
			END;
		END CASE;
	COMMIT;
	SET info = 'success';
	SET code = 0;
END $$
DELIMITER ;
