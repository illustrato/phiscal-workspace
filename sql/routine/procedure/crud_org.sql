-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `crud_org`
(
	IN tenantId	BIGINT UNSIGNED,
	IN name		VARCHAR(45),
	IN struct	VARCHAR(21),
	IN regno	VARCHAR(21),
	IN commission	VARCHAR(85),
	IN web		VARCHAR(35),
	IN dba		VARCHAR(65),
	IN intel	VARCHAR(255),
	IN slogan	VARCHAR(255),
	IN img		VARCHAR(5),
	IN modId	CHAR(2) CHARSET UTF8,
	IN client 	CHAR(12),
	OUT code 	INT,
	OUT info 	TEXT
)
BEGIN
	DECLARE tempId VARCHAR(255) DEFAULT tenant(tenantId);
	DECLARE orgId BIGINT UNSIGNED DEFAULT IF(tempId REGEXP '^[0-9]+$', tempId, 0);
	DECLARE hasLogo BOOLEAN DEFAULT FALSE;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;
		CASE orgId
			WHEN 0 THEN
				BEGIN
					INSERT INTO organization(NAME, STRUCT_ID, REGNO, REGISTRA, WEBSITE, MOTTO) VALUES(name, struct, regno, commission, web, slogan);
					SET orgId = LAST_INSERT_ID();
					INSERT INTO admin VALUES(client, 'A', orgId);
					INSERT INTO tenant(ORG_ID,TYPE) VALUES(orgId,'O');
					SET tenantId = LAST_INSERT_ID();

					IF modId IS NOT \N THEN
						INSERT INTO subscription VALUES(tenantId, modId, NOW(), 1, TRUE);
					END IF;
				END;
			ELSE
				BEGIN
					UPDATE organization
					SET
						NAME = name,
						STRUCT_ID = struct,
						REGNO = regno,
						REGISTRA = commission,
						WEBSITE = web,
						DBA = dba,
						DESCRIPTION = intel,
						MOTTO = slogan
					WHERE
						ORG_ID = orgId;
					SELECT COUNT(*) FROM logo WHERE ORG_ID = orgId AND MAIN INTO hasLogo;
				END;
		END CASE;

		IF img IS NOT \N THEN
			IF hasLogo THEN
				UPDATE logo SET EXT = img WHERE ORG_ID = orgId AND MAIN;
			ELSE
				INSERT INTO logo(ORG_ID, EXT, MAIN) VALUES(tenantId, img, TRUE);
			END IF;
		END IF;
	COMMIT;
	SET code = 0;
	SELECT organization FROM v_org_compact WHERE ORG_ID = orgId INTO info;
END $$
DELIMITER ;
