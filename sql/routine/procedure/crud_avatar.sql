-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE PROCEDURE `crud_avatar`
(
	IN target	CHAR(12) CHARSET UTF8,
	IN tenantId	BIGINT UNSIGNED,
	IN pic		VARCHAR(5),
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE org BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE admin_name VARCHAR(41) DEFAULT(SELECT fullname FROM v_activeUser WHERE id = client);
	DECLARE isEmployed BOOLEAN;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;

	CASE target
	WHEN 'self' THEN
		BEGIN
			SELECT AVATAR FROM user WHERE USER_ID = client INTO info;
			UPDATE user SET AVATAR = pic WHERE USER_ID = client;
		END;
	ELSE
		BEGIN
			IF NOT auth(tenantId, client) THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
			END IF;
			SELECT COUNT(*) FROM employee WHERE USER_ID = target AND ORG_ID = org INTO isEmployed;
			IF NOT isEmployed THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "This is No Employee", MYSQL_ERRNO = 3001;
			END IF;
			UPDATE user SET AVATAR = pic WHERE USER_ID = target;
			INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(target, 'event', 'fa fa-user text-success', CONCAT(admin_name ,IF(pic IS \N || pic = '',' removed',' uploaded'),' your profile picture.'));
			SELECT * FROM v_staff  WHERE organization = org;
		END;
	END CASE;

	COMMIT;
	SET code = 0;
END $$
DELIMITER ;
