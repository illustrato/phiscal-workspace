-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `verify`
(
	jwt		TEXT,
	tenantId	BIGINT UNSIGNED
) RETURNS TEXT
BEGIN
	DECLARE session TEXT DEFAULT (SELECT user FROM v_session u INNER JOIN token t USING (USER_ID) WHERE TOKEN = jwt);
	DECLARE uid CHAR(12) CHARSET UTF8 DEFAULT (SELECT USER_ID FROM token WHERE TOKEN = jwt);
	DECLARE business TEXT DEFAULT (SELECT business FROM v_tenant WHERE tenant_id = tenantId AND auth(tenantId, uid));
	DECLARE modules TEXT DEFAULT (SELECT modules FROM v_tenant WHERE tenant_id = tenantId AND auth(tenantId, uid));
	DECLARE whispers TEXT DEFAULT (SELECT JSON_ARRAYAGG(feed ORDER BY d.`TIME` DESC) FROM v_dopamine d INNER JOIN token t USING (USER_ID) WHERE TOKEN = jwt);
	DECLARE unread SMALLINT UNSIGNED DEFAULT(SELECT COUNT(*) FROM notification n INNER JOIN token USING (`USER_ID`) WHERE NOT `HAVE_READ` AND TOKEN = jwt);

	DECLARE feeds TEXT DEFAULT CONCAT('"unread":', unread, ',"records":', IF(whispers IS \N, '[]', whispers));

	SET session = IF(session IS \N, FALSE, session);
	SET business = IF(business IS \N, FALSE, business);
	SET modules = IF(modules IS \N, '[]', modules);

	RETURN CONCAT('{"user":', session, ', "tenant":', business, ', "rank":', userprofile(uid,tenantId), ', "modules":', modules,', "feed":{', feeds, '}}');

END $$
DELIMITER ;
