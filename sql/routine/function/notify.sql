-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `notify`
(
	client	CHAR(12) CHARSET UTF8,
	class	VARCHAR(5),
	img	VARCHAR(32),
	alert	CHAR(2),
	content	VARCHAR(255)
)
RETURNS BOOLEAN DETERMINISTIC
BEGIN
	DECLARE feeds TINYINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM notification WHERE USER_ID = client AND TYPE = class);
	IF feeds = 10 THEN
		DELETE FROM notification WHERE USER_ID = client AND TYPE = class ORDER BY TIME LIMIT 1;
	END IF;
	INSERT INTO notification(USER_ID,TYPE,ICON,MOD_ID,MESSAGE) VALUES(client,class,img,alert,content);
	RETURN TRUE;
END$$
DELIMITER ;
