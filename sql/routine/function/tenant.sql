-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `tenant`
(
	tenantId	BIGINT UNSIGNED
)
RETURNS VARCHAR(255)
BEGIN
	DECLARE tenantType CHAR(1) DEFAULT (SELECT TYPE FROM tenant WHERE TENANT_ID = tenantId);
	DECLARE primaryKey VARCHAR(255);

	IF tenantId < 1  || tenantType IS \N THEN
		RETURN 0;
	END IF;

	CASE tenantType
		WHEN 'O' THEN
			SELECT ORG_ID FROM tenant WHERE TENANT_ID = tenantId INTO primaryKey;
		WHEN 'U' THEN
			SELECT USER_ID FROM tenant WHERE TENANT_ID = tenantId INTO primaryKey;
	END CASE;

	RETURN primaryKey;
END$$
DELIMITER ;
