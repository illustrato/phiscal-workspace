-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `caste`
(
	string	VARCHAR(255),
	target	CHAR(2) CHARSET UTF8
)
RETURNS CHAR(1) CHARSET UTF8
BEGIN
	DECLARE item CHAR(1) CHARSET UTF8 DEFAULT 'G';
	SET string = TRIM(string);
	CASE target
	WHEN 'ZA' THEN
		BEGIN
			IF LENGTH(string) < 13 THEN RETURN item; END IF;
			RETURN IF(SUBSTR(string, 7, 4) >= 5000, 'A', 'B');
		END;
	WHEN '--' THEN
		BEGIN
			IF LENGTH(string) < 12 THEN RETURN item; END IF;
			RETURN SUBSTR(string, 6, 1);
		END;
	ELSE RETURN item;
	END CASE;
END $$
DELIMITER ;
