-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `auth`
(
	tenantId	BIGINT UNSIGNED,
	client	CHAR(12) CHARSET UTF8
)
RETURNS BOOLEAN
BEGIN
	DECLARE target CHAR(1) DEFAULT (SELECT TYPE FROM tenant WHERE TENANT_ID = tenantId);
	DECLARE uid CHAR(12) CHARSET UTF8;
	DECLARE decider BOOLEAN DEFAULT FALSE;

	CASE target
	WHEN 'U' THEN
		BEGIN
			SELECT USER_ID FROM tenant WHERE TENANT_ID = tenantId INTO uid;
			SELECT uid = client INTO decider;
		END;
	WHEN 'O' THEN
		SELECT
			(SELECT COUNT(*) FROM admin WHERE ORG_ID = tenant(tenantId) AND USER_ID = client)
			||
			(SELECT COUNT(*) FROM employee WHERE ORG_ID = tenant(tenantId) AND USER_ID = client)
		INTO decider;
	ELSE
		RETURN decider;
	END CASE;

	RETURN decider;
END $$
DELIMITER ;
