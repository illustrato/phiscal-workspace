-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `userprofiles`
(
	uId VARCHAR(12) CHARSET UTF8
)
RETURNS TEXT
BEGIN
	DECLARE array TEXT DEFAULT '';
	DECLARE done, matched BOOLEAN DEFAULT FALSE;

	DECLARE temp_rank VARCHAR(15);
	DECLARE temp_stage TINYINT;

	DECLARE cursor_rank CURSOR FOR SELECT NAME FROM profile WHERE PROFILE_ID <> 'Z' ORDER BY PRECEDENCE DESC;
	DECLARE cursor_stage CURSOR FOR SELECT PRECEDENCE FROM profile WHERE PROFILE_ID <> 'Z' ORDER BY PRECEDENCE DESC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cursor_rank;
	OPEN cursor_stage;

	read_loop: LOOP
		FETCH cursor_rank INTO temp_rank;
		FETCH cursor_stage INTO temp_stage;
		IF done THEN
			LEAVE read_loop;
		END IF;

		CASE temp_stage
			WHEN 4 THEN SELECT COUNT(USER_ID) FROM tech WHERE USER_ID = uId INTO matched;
			WHEN 3 THEN SELECT COUNT(USER_ID) FROM admin WHERE USER_ID = uId INTO matched;
			WHEN 2 THEN SELECT COUNT(USER_ID) FROM employee WHERE USER_ID = uId INTO matched;
			WHEN 1 THEN SELECT COUNT(USER_ID) FROM tenant WHERE USER_ID = uId INTO matched;
		END CASE;

		IF matched THEN
			SET array = CONCAT(array,',"',temp_rank,'"');
		END IF;
	END LOOP;

	CLOSE cursor_rank;
	CLOSE cursor_stage;

	IF LENGTH(array) = 0 THEN
		SELECT CONCAT(',"',NAME,'"') FROM profile WHERE PROFILE_ID = 'Z' INTO array;
	END IF;

	RETURN CONCAT('[',SUBSTRING(array,2),']');
END$$
DELIMITER ;
