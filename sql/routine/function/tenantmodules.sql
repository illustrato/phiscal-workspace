-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `tenantmodules`
(
	tenantId	BIGINT UNSIGNED
)
RETURNS TEXT
NOT DETERMINISTIC
BEGIN
	DECLARE list TEXT DEFAULT (
		SELECT JSON_ARRAYAGG(
			JSON_OBJECT(
				"mod_id", mod_id,
				"name", name,
				"description", description,
				"webclient", webclient,
				"logo", logo,
				"days", days,
				"active", active
			)
		)
		FROM v_module
		WHERE tenant_id = tenantId
	);
	RETURN IF(list IS \N, '[]',list);
END$$
DELIMITER ;
