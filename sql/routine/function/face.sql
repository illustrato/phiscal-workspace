-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `face`
(
	subject	BIGINT UNSIGNED
)
RETURNS VARCHAR(255)
BEGIN
	DECLARE target VARCHAR(6) DEFAULT (SELECT type FROM tenant WHERE TENANT_ID = subject);
	DECLARE id VARCHAR(90);
	DECLARE file VARCHAR(255);

	CASE target
		WHEN 'U' THEN
		BEGIN
			SELECT USER_ID FROM tenant WHERE TENANT_ID = subject INTO id;
			SELECT CONCAT('img/avatar/', id, '.', AVATAR) FROM user WHERE USER_ID = id INTO file;
		END;
		WHEN 'O' THEN
		BEGIN
			SELECT ORG_ID FROM tenant WHERE TENANT_ID = subject INTO id;
			SELECT CONCAT('img/logo/', id, '/', LOGO_NO,'.',EXT) FROM logo WHERE ORG_ID = id AND MAIN INTO file;
		END;
	END CASE;

	RETURN file;
END$$
DELIMITER ;
