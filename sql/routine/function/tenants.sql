-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `tenants`
(
	uId	CHAR(12) CHARSET UTF8
)
RETURNS VARCHAR(255)
BEGIN
	RETURN IFNULL((SELECT JSON_ARRAYAGG(TENANT_ID) FROM v_user_tenant WHERE `USER_ID` = uId),'[]');
END$$
DELIMITER ;
