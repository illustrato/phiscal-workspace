-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `readfeeds`
(
	client	CHAR(12) CHARSET UTF8
) RETURNS TEXT
BEGIN
	DECLARE whispers TEXT;

	UPDATE notification SET HAVE_READ = TRUE WHERE USER_ID = client;
	SELECT JSON_ARRAYAGG(feed ORDER BY `TIME` DESC) FROM v_dopamine WHERE USER_ID = client INTO whispers;

	RETURN whispers;

END $$
DELIMITER ;
