-- --defaults-group-suffix=Phiscal
-- -t
--
DELIMITER $$
CREATE OR REPLACE FUNCTION `dob`
(
	idno	VARCHAR(32),
	nation	CHAR(2),
	birthday DATE
)
RETURNS DATE
BEGIN
	SET idno = TRIM(idno);


	CASE nation
	WHEN 'ZA' THEN
		IF idno REGEXP '^[0-9]{13}$' THEN
			SET birthday = STR_TO_DATE(SUBSTRING(idno,1,6),'%y%m%d');
		END IF;
	ELSE
	BEGIN
	END;
	END CASE;

	RETURN birthday;
END$$
DELIMITER ;
