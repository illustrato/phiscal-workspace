# Definitions

> **Administrator**   
> The type of user that registered an organization with Phiscal, or is selected by another administrator to be administering one.

> **Anonymous User**  
> The type of user that is not logged into a Phiscal Account  

> **Freelance Agent**  
> An informal business structure where the individual itself is the business.  

> **General User**  
> A user logged in ordinary Phiscal Account 

> **Module**  
> An individual web application with functionality tailored to support specific business rules, powered by the Phiscal API.

> **Organization**   
> Any formal business structure, all the way from a sole trader, partnership, to a large business enterprise.

> **Phiscal Team**  
> The group of programmers and management behind the development of the Phiscal Project  

> **Tenant**  
> The Phiscal application instance assigned to each business in order for it to be able use system.

# Project Description

Phiscal is a general purpose **Business Information System**. It delivers functionality specific to business rules through subsystems, in the Phiscal context, called **"Modules"**. 
 
Phiscal serves as an **Identity Provider** powering the user authentication and authorization of these Modules, and an API providing common functionality to most Modules.  
This functionality includes organizational **Branch**, **Department**, **Employee**, and **Roles** database entities.

## Target Users

* Anonymous User
* General User
* Freelance Agent
* Organization Employee
* Organization Administrator

## Functionality

> **Make Inquiry**  
> This defines the ability for registered and non-registered users to inquire information about the Phiscal project to the Phiscal team.
>  
> **Register Business with System**  
> This defines the function to add an organization, or register a user to trade as an individual. The output of this use case is the creation of the **Tenant** associated with business in question, whether organization or freelance agent.
>  
> **Update Business Information**  
> Not all business fields are available to input during registration, this provides the ability to add such fields, and keep to date changed information, or remove fields no longer relevant. For an organization, this includes logo, branch, department, employee, and role CRUD operations.  
>
> **Subscribe Module**  
> This defines an association of a **Tenant** with a **Module** it can use for the specified period. The duration is normally 30 days. The subscription, however, can still be canceled or paused by user before it expires.  
>  
> **Delete Organization**  
> This defines the ability for administrators to unanimously delete their organization off Phiscal. All associated employees will be notified of action, and will have their employee status purged.  

### Per User

| Use Case		| Anonymous User	| General User	| Freelance Agent	| Employee	| Administrator	|
|-----------------------|:---------------------:|:-------------:|:---------------------:|:-------------:|:-------------:|
| Make Inquiry		|	X		|	X	|	X		|	X	|	X	|
| Sign Up		|	X		|		|			|		|		|
| Update Personal Info	|			|	X	|	X		|		|	X	|
| Register Business	|			|	X	|	X		|	X	|	X	|
| Branch CRUD		|			|		|			|		|	X	|
| Employee CRUD		|			|		|			|		|	X	|
| Logo CRUD		|			|		|			|		|	X	|
| Subscribe Module	|			|		|	X		|		|	X	|
| Cancel Subscription	|			|		|	X		|		|	X	|
| Delete Organization	|			|		|			|		|	X	|



## Technology

Phiscal is a database, accessed through a RESTful API built with [Phalcon](https://phalcon.io), a *php* framework served as a C extension.  
Endpoint routes are divided between **private** and **public** routes.  
Private routes are those that require the **Authorization** HTTP header, with a JWT using the Bearer Schema as a value.  
Routes that operate on a particular Tenant require the Tenant ID to be passed as a value in the **Tenant** HTTP header.  
 
Users interact with system via a [web client](https://www.phiscal.site) built with VueJS SPA technology, and an Android app built with Java + XML. 
